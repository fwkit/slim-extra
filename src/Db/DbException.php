<?php
/**
 * @author   Fung Wing Kit <wengee@gmail.com>
 * @version  2019-01-08 12:27:04 +0800
 */
namespace SlimExtra\Db;

use SlimExtra\Exception;

class DbException extends Exception
{
}
