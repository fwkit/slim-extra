<?php
/**
 * @author   Fung Wing Kit <wengee@gmail.com>
 * @version  2019-01-08 12:26:56 +0800
 */
namespace SlimExtra;

class Exception extends \Exception
{
}
