<?php
/**
 * @author   Fung Wing Kit <wengee@gmail.com>
 * @version  2019-01-18 16:09:31 +0800
 */
namespace SlimExtra\Swoole\Db;

use SlimExtra\Exception;

class DbException extends Exception
{
}
